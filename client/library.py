import json
import pickle
import socket

from entities.messageHandler import Message
from protocols.protocol import Protocol
from constants import constant
from exceptions.client import ClientException

PORT = constant.PORT
FORMAT = constant.FORMAT
HOST = constant.HOST


class Client:
    def __init__(self):
        pass

    def GenerateSocket(self):
        client_socket = socket.socket()
        return client_socket

    def ConnectServer(self, client_socket):
        try:
            client_socket.connect((HOST, PORT))
        except ClientException as e:
            e.GetErrorMessage()

    def Send(self, client_socket, msg):
        client_socket.send(pickle.dumps(msg))

    def Recieve(self, client_socket):
        return pickle.loads(client_socket.recv(1024))

    def GetImqProtocol(self, data, source, destination):
        return Protocol(data, source, destination)

    def ShowMenu(self):
        print("Enter R for register\n")
        print("Enter L for Login\n")
        print("Enter S for SignOut\n")

    def SendRecieve(self, client_socket, msg):
        self.Send(client_socket, msg)
        imq_protocol = self.Recieve(client_socket)
        message = imq_protocol.GetData()
        return message

    def ClientSpecficMenu(self):
        print("Enter T to show Topics\n")
        print("Enter Register to register the topic\n")
        print("Enter GT to get Registered Topics\n")
        print("Enter P to publish or consume message\n")
        print("Enter Return for back\n")

    def ShowTopicMenu(self, topic, client_socket, source, destination):
        print("Enter Pub to Publish Message\n")
        print("Enter Sub to Consume Message\n")
        print("Enter Return to get back")
        while True:
            request = input("Enter response Pub=Publish Sub=Consume Return=back\n")
            if request == constant.PUBLISH:
                data = input("Enter Message to publish\n")
                message_obj = Message()
                formatted_message = message_obj.BuildMessage(data)
                message_to_server = request + '@' + json.dumps(formatted_message) + '@' + topic
                protocol = self.GetImqProtocol(message_to_server, source, destination)
                received_response = self.SendRecieve(client_socket, protocol)
                print(received_response)
            elif request == constant.SUBSCRIBE:
                requested_msg = request + '@' + topic
                protocol = self.GetImqProtocol(requested_msg, source, destination)
                received_response = self.SendRecieve(client_socket, protocol)
                if isinstance(received_response, str):
                    print(received_response)
                else:
                    print("Message Is ...............\n")
                    print(received_response[0])
                    print('\n')
            elif request == constant.RETURN:
                break
            else:
                print("Enter Valid options")
