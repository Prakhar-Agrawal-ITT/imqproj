from client.library import Client
from constants import constant

if __name__ == "__main__":
    client = Client()
    socket = client.GenerateSocket()
    client.ConnectServer(socket)
    client.ShowMenu()
    is_authenticated = False
    response_protocol = client.Recieve(socket)
    res = response_protocol.GetData()
    source = response_protocol.GetSource()
    destination = response_protocol.GetDestination()

    while True:
        msg = input("Enter Message R=Register L=Login S=SignOut\n")
        if msg == constant.REGISTER:
            id = input('Enter Client Id:\n')
            password = input('Enter Password\n')
            confirm_password = input('Enter Password Again\n')
            msg = msg + "@" + id + '@' + password + '@' + confirm_password
        elif msg == constant.LOGIN:
            id = input('Enter Client Id:\n')
            password = input('Enter Password\n')
            msg = msg + "@" + id + '@' + password
        elif msg == constant.SIGNOUT:
            imq_protocol = client.GetImqProtocol(msg, source, destination)
            client.Send(socket, imq_protocol)
            break
        else:
            if not is_authenticated:
                print('Please Login or Register')
                client.ShowMenu()
                continue
        if msg != '':
            imq_protocol = client.GetImqProtocol(msg, source, destination)
            client.Send(socket, imq_protocol)
            res = client.Recieve(socket).GetData()
            print(res)
        if res == 'Login Successfully':
            is_authenticated = True
            client.ClientSpecficMenu()
            while True:
                request_message = input("Enter your choice between T=Topics, Register, GT=RegisterTopics, "
                                        "P=Publish/Consume Return=back\n")
                if request_message == constant.TOPICS:
                    imq_protocol = client.GetImqProtocol(request_message, source, destination)
                    response = client.SendRecieve(socket, imq_protocol)
                    print("Topics ......\n")
                    for topic in response:
                        print(topic[0])
                    print('\n')
                elif request_message == constant.TOPICREGISTER:
                    message = input("Enter Topic Name to Register\n")
                    request_message = request_message + '@' + message
                    imq_protocol = client.GetImqProtocol(request_message, source, destination)
                    response = client.SendRecieve(socket, imq_protocol)
                    print(response)
                    print("\n")

                elif request_message == constant.GETTOPICS:
                    imq_protocol = client.GetImqProtocol(request_message, source, destination)
                    registered_topics = client.SendRecieve(socket, imq_protocol)
                    if isinstance(registered_topics, str):
                        print(registered_topics)
                    else:
                        print("Registered Topics ......\n")
                        for topic in registered_topics:
                            print(topic[0][0])
                    print('\n')
                elif request_message == constant.PUSH:
                    print("Choose from Registered Topics ......\n")
                    imq_protocol = client.GetImqProtocol('GT', source, destination)
                    registered_topics = client.SendRecieve(socket, imq_protocol)
                    if isinstance(registered_topics, str):
                        print(registered_topics)
                    else:
                        print("Registered Topics ......\n")
                        for topic in registered_topics:
                            print(topic[0][0])
                        print('\n')
                        topic_name = input("Choose Topic Name from registered topics to publish or consume message\n")
                        client.ShowTopicMenu(topic_name, socket, source, destination)
                elif request_message == constant.RETURN:
                    break

    socket.close()
