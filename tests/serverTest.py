import unittest

from dbLayer.database import Database
from protocols.protocol import Protocol
from server.library import Server


class ServerTest(unittest.TestCase):
    def test_DbObject(self):
        server = Server()
        db_obj = server.GetDbObject()
        db_test = Database()
        self.assertEqual(type(db_test), type(db_obj))

    def test_ImqProtocol(self):
        server = Server()
        imq_protocol = server.GetImqProtocol('testing', '127.0.0.1:8787', '127.0.0.1:9898')
        imq_protocol_obj = Protocol('testing', '127.0.0.1:8787', '127.0.0.1:9898')
        self.assertEqual(type(imq_protocol), type(imq_protocol_obj))


if __name__ == '__main__':
    unittest.main()
