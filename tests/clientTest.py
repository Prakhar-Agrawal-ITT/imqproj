import socket
import unittest

from client.library import Client


class ClientTest(unittest.TestCase):
    def test_Sockets(self):
        client = Client()
        client_socket = client.GenerateSocket()
        client_socket.close()
        test_socket = socket.socket()
        test_socket.close()
        self.assertEqual(type(client_socket), type(test_socket))

    def test_ConnectServer(self):
        client = Client()
        client_socket = client.GenerateSocket()
        connect = client_socket.connect(('localhost', 4040))
        test_socket = socket.socket()
        connect_test = test_socket.connect(('localhost', 4040))
        client_socket.close()
        test_socket.close()
        self.assertEqual(type(connect), type(connect_test))

    def test_ImqProtocol(self):
        client = Client()
        imq_protocol = client.GetImqProtocol('Testing', '127.0.0.1:8787', '127.0.0.1:9898')
        self.assertEqual('127.0.0.1:8787', imq_protocol.source)


if __name__ == '__main__':
    unittest.main()
