# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
  Message Queue Application. First Client needs to login or register to either publish or subscribe the message to a 
  particular topic. Publisher can publish the message on the choice of topic. Subscriber can consume the message
  from the topic. There will be expiration time of message such that if not consumed till that time will send to dead-latter
  queue.
* Version
* [Learn Markdown](https://bitbucket.org/Prakhar-Agrawal-ITT/imqproj/src)

### How do I get set up? ###

* Summary of set up
  need of Python above 3.x version and mysql db is required
* Configuration
* Dependencies
* Database configuration
  MySql db
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
  Prakhar Agrawal
* Other community or team contact