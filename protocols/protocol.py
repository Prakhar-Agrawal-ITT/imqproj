class Protocol:
    def __init__(self, data, source, destination):
        self.data = data
        self.source = source
        self.destination = destination

    def GetData(self):
        return self.data

    def GetSource(self):
        return self.source

    def GetDestination(self):
        return self.destination
