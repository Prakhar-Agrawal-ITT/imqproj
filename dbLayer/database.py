import json
from datetime import datetime

import mysql.connector
from dbLayer.config import config


class Database:
    def __init__(self):
        pass

    def GetParams(self):
        params = config()
        return params

    def GetConnection(self):
        try:
            connection = mysql.connector.connect(**self.GetParams())
            return connection
        except Exception as error:
            print(error)

    def GetCursor(self, connection):
        cur = connection.cursor()
        return cur

    def CreateClientTable(self, cursor, clientId):
        table_name = "Client" + clientId
        query = """CREATE TABLE """ + table_name + """ (
                  serial_no  INT PRIMARY KEY AUTO_INCREMENT,
                   timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
                  data VARCHAR(200)
                  )"""
        try:
            cursor.execute(query)
        except Exception as error:
            print(error)
        pass

    def SaveInClientTable(self, cursor, clientId, data):
        table_name = 'Client' + clientId
        query = ("INSERT INTO {} (serial_no, timestamp, data) VALUES(%s, %s, %s)".format(table_name))
        try:
            now = datetime.now()
            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            data = (dt_string, "NULL", str(data))
            cursor.execute(query, data)
        except Exception as error:
            print(error)

    def SaveInRootTable(self, cursor, clientId):
        table_name = 'Client' + clientId
        query = "INSERT INTO enrolledClient (ID, tableName) VALUES(%s, %s)"
        data = (clientId, table_name)
        cursor.execute(query, data)

    def IsTableExist(self, cursor, clientId):
        table_name = 'Client' + clientId
        query = ("SHOW TABLES FROM imqdb LIKE '{}'".format(table_name))
        print(query)
        cursor.execute(query)

        if len(cursor.fetchall()) > 0:
            return True
        else:
            return False

    def GetTopics(self, cursor):
        table_name = 'topic'
        query = ("SELECT name FROM {}".format(table_name))
        cursor.execute(query)
        return cursor.fetchall()

    def RegisterToTopic(self, cursor, topic_name, client_id):
        table_name = 'topic'
        query = ("SELECT id FROM {} WHERE name =  '{}'".format(table_name, topic_name))
        cursor.execute(query)
        topic_obj = cursor.fetchall()
        if cursor.rowcount > 0:
            topic_id = topic_obj[0][0]
            query = ("SELECT id FROM subscribertopicmap WHERE topicid =  '{}' AND clientid = '{}'".format(topic_id,
                                                                                                          client_id))
            cursor.execute(query)
            cursor.fetchall()
            if cursor.rowcount > 0:
                return "Already Registered!!"
            data = (client_id, topic_id)
            query = "INSERT INTO subscribertopicmap (clientid, topicid) VALUES (%s, %s)"
            cursor.execute(query, data)
            return "Registered Successfully!!"
        else:
            return "Topic Not Present Try again!!"

    def GetRegisteredTopics(self, cursor, client_id):
        # Condition for no regisreterd topic need to check
        table_name = 'subscribertopicmap'
        query = ("SELECT topicid FROM {} WHERE clientid =  '{}'".format(table_name, client_id))
        cursor.execute(query)
        topic_id = cursor.fetchall()
        topic_name = []
        if cursor.rowcount > 0:
            for id in topic_id:
                query = ("SELECT name FROM topic WHERE id =  '{}'".format(id[0]))
                cursor.execute(query)
                topic_name.append(cursor.fetchall())
            return topic_name
        else:
            return "No Registered topics available. Type Register to register!!"

    def InsertInMessage(self, cursor, topic_name, client_id, message):
        table_name = 'topic'
        query = ("SELECT id FROM {} WHERE name =  '{}'".format(table_name, topic_name))
        cursor.execute(query)
        topic_obj = cursor.fetchall()
        if cursor.rowcount > 0:
            topic_id = topic_obj[0][0]
            data = (client_id, topic_id)
            query = "INSERT INTO publishertopicmap (clientid, topicid) VALUES (%s, %s)"
            cursor.execute(query, data)
            converted_msg = json.loads(message)
            msg = converted_msg['msg']
            created_at = converted_msg['createdAt']
            expired_at = converted_msg['expiredAt']
            data = (msg, client_id, topic_id, created_at, expired_at)
            query = "INSERT INTO messages (data, clientid, topicid, createdAt, expiredAt) VALUES (%s, %s, %s, %s, %s)"
            cursor.execute(query, data)
            return "Message Published"
        else:
            return "Topic Not Present. Press Return and Try again!!"

    def GetMessage(self, cursor, client_id, topic_name):
        table_name = 'topic'
        query = ("SELECT id FROM {} WHERE name =  '{}'".format(table_name, topic_name))
        cursor.execute(query)
        topic_obj = cursor.fetchall()
        if cursor.rowcount > 0:
            topic_id = topic_obj[0][0]
            query = ("SELECT data FROM messages WHERE clientid = '{}' AND topicid = '{}' AND status = 'True'"
                     "ORDER BY id DESC".format(client_id, topic_id))
            cursor.execute(query)
            messages = cursor.fetchone()
            if cursor.rowcount > 0:
                return messages
            else:
                return "No Message to show!!"
        else:
            return "Invalid Topic. Press Return and try again!!"

    def SetStatus(self, client_id, topic_name, data, cursor):
        table_name = 'topic'
        query = ("SELECT id FROM {} WHERE name =  '{}'".format(table_name, topic_name))
        cursor.execute(query)
        topic_obj = cursor.fetchall()
        if cursor.rowcount > 0:
            topic_id = topic_obj[0][0]
            query = ("UPDATE messages SET status = 'False' WHERE clientid = '{}' AND topicid = '{}' AND data = '{}'"
                     .format(client_id, topic_id, data))
            cursor.execute(query)

    def CloseConnection(self, connection):
        connection.close()
