class ClientException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def GetErrorMessage(self):
        print(self.msg)
