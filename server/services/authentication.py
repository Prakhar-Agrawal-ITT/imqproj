from dbLayer.database import Database


class Authentication:
    def __init__(self):
        pass

    def GetDatabaseObject(self):
        db = Database()
        return db

    def Login(self, id_, password_):
        db = self.GetDatabaseObject()
        db_conn = db.GetConnection()
        cursor = db.GetCursor(db_conn)
        table_name = 'client'
        query = ("SELECT id, password FROM {} WHERE id = %s AND password = %s".format(table_name))
        try:
            id = id_
            password = password_
            data = (id, password)
            cursor.execute(query, data)
            result = cursor.fetchone()
            # print(result)
            db.CloseConnection(db_conn)
            # print(type(id))
            if result is None:
                return False
            if result[0] == id and result[1] == password:
                return True
            else:
                return False
        except Exception as error:
            print(error)
