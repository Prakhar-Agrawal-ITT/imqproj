from dbLayer.database import Database


class Register:
    def __init__(self):
        pass

    def GetDatabaseObject(self):
        db = Database()
        return db

    def RegisterClient(self, id, password):
        db = self.GetDatabaseObject()
        db_conn = db.GetConnection()
        cursor = db.GetCursor(db_conn)
        table_name = 'client'
        query = ("INSERT INTO {} (id, password) VALUES(%s, %s)".format(table_name))
        try:
            user_id = id
            user_password = password
            data = (user_id, user_password)
            cursor.execute(query, data)
            db_conn.commit()
            db.CloseConnection(db_conn)
        except Exception as error:
            print(error)

    def IsValidate(self, password, confirm_Password):
        if password == confirm_Password:
            return True
        else:
            return False
