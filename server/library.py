import pickle
import socket

from dbLayer.database import Database
from exceptions.server import ServerException
from protocols.protocol import Protocol
from server.services.authentication import Authentication
from server.queueHandler import Queue
from server.services.register import Register
from loggers.loggers import Loggers
from constants import constant

PORT = constant.PORT
FORMAT = constant.FORMAT
HOST = constant.HOST


class Server:
    def __init__(self):
        pass

    def GenerateSocket(self):
        server_socket = socket.socket()
        return server_socket

    def Bind(self, server_socket):
        try:
            server_socket.bind((HOST, PORT))
        except ServerException as e:
            e.GetErrorMessage()

    def GetLoggerObject(self):
        log = Loggers()
        return log

    def GetDbObject(self):
        db = Database()
        return db

    def GetImqProtocol(self, data, source, destination):
        return Protocol(data, source, destination)

    def Recieve(self, conn):
        data = conn.recv(1024)
        imq_protocol = pickle.loads(data)
        return imq_protocol.GetData()

    def Send(self, conn, imq_protocol):
        conn.send(pickle.dumps(imq_protocol))

    def GetTopics(self):
        db = self.GetDbObject()
        db_conn = db.GetConnection()
        cursor = db.GetCursor(db_conn)
        return db.GetTopics(cursor)

    def RegisterToTopic(self, topic_name, client_id):
        db = self.GetDbObject()
        db_conn = db.GetConnection()
        cursor = db.GetCursor(db_conn)
        response = db.RegisterToTopic(cursor, topic_name, client_id)
        db_conn.commit()
        db.CloseConnection(db_conn)
        return response

    def GetRegisteredTopics(self, client_id):
        db = self.GetDbObject()
        db_conn = db.GetConnection()
        cursor = db.GetCursor(db_conn)
        response = db.GetRegisteredTopics(cursor, client_id)
        return response

    def SaveInMessage(self, client_id, topic_name, message):
        db = self.GetDbObject()
        db_conn = db.GetConnection()
        cursor = db.GetCursor(db_conn)
        response = db.InsertInMessage(cursor, topic_name, client_id, message)
        db_conn.commit()
        db.CloseConnection(db_conn)
        return response

    def GetMessage(self, client_id, topic_name):
        db = self.GetDbObject()
        db_conn = db.GetConnection()
        cursor = db.GetCursor(db_conn)
        response = db.GetMessage(cursor, client_id, topic_name)
        if isinstance(response, str):
            return response
        else:
            queue = Queue(response)
            self.UpdateStatus(client_id, topic_name, queue.getData())
            return response

    def UpdateStatus(self, client_id, topic_name, data):
        db = self.GetDbObject()
        db_conn = db.GetConnection()
        cursor = db.GetCursor(db_conn)
        db.SetStatus(client_id, topic_name, data, cursor)
        db_conn.commit()

    def HandleClient(self, conn, addr):
        print(f"New Connection {addr} pinged")
        imq_protocol = self.GetImqProtocol('Server is working', 'localhostPort',
                                           str(addr[0]) + ':' + str(addr[1]))
        self.Send(conn, imq_protocol)
        connected = True
        client_id = None
        response = ""
        while connected:
            msg = self.Recieve(conn)
            data = msg
            log = self.GetLoggerObject()
            file_name = str(addr[0]) + "_" + str(addr[1]) + ".txt"
            log.CreateFile(file_name, msg)
            if msg.split('@')[0] == constant.REGISTER:
                id = msg.split('@')[1]
                password = msg.split('@')[2]
                confirm_password = msg.split('@')[3]
                authentication = Register()
                if authentication.IsValidate(password, confirm_password):
                    authentication.RegisterClient(id, password)
                    response = 'Registartion Successfully'
                else:
                    response = 'Password and Confirm Password should match'
            elif msg.split('@')[0] == constant.LOGIN:
                id = msg.split('@')[1]
                password = msg.split('@')[2]
                authentication = Authentication()
                if authentication.Login(id, password):
                    response = 'Login Successfully'
                    client_id = id
                else:
                    response = 'Login Failed'
            elif msg == constant.SIGNOUT:
                print("Logout Successfully")
                conn.close()
                exit()
            elif msg == constant.TOPICS:
                response = self.GetTopics()
            elif msg.split('@')[0] == constant.TOPICREGISTER:
                topic_name = msg.split('@')[1]
                response = self.RegisterToTopic(topic_name, client_id)
            elif msg == constant.GETTOPICS:
                response = self.GetRegisteredTopics(client_id)
            elif msg.split('@')[0] == constant.PUBLISH:
                topic_name = msg.split('@')[2]
                message = msg.split('@')[1]
                response = self.SaveInMessage(client_id, topic_name, message)
            elif msg.split('@')[0] == constant.SUBSCRIBE:
                topic_name = msg.split('@')[1]
                response = self.GetMessage(client_id, topic_name)
            else:
                print(f"{addr} {msg}")
                response = data
                db = self.GetDbObject()
                db_conn = db.GetConnection()
                cursor = db.GetCursor(db_conn)
                if not (db.IsTableExist(cursor, client_id)):
                    db.CreateClientTable(cursor, client_id)
                    db.SaveInRootTable(cursor, client_id)
                    db_conn.commit()
                db.SaveInClientTable(cursor, client_id, data)
                db_conn.commit()
                db.CloseConnection(db_conn)
            imq_protocol.data = response
            self.Send(conn, imq_protocol)
        conn.close()
