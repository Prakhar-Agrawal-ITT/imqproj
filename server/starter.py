import threading

from entities.deadLatterQueue import DeadLatter
from server.library import Server

if __name__ == "__main__":
    server = Server()
    print("Server is starting")
    dead = DeadLatter()
    dead.InsertInQueue()
    socket = server.GenerateSocket()
    server.Bind(socket)
    socket.listen(5)
    while True:
        connection, address = socket.accept()
        thread = threading.Thread(target=server.HandleClient, args=(connection, address))
        thread.start()
