from dbLayer.database import Database


class DeadLatter:
    def __init__(self):
        pass

    def InsertInQueue(self):
        db = Database()
        db_conn = db.GetConnection()
        cursor = db.GetCursor(db_conn)
        table_name = 'messages'
        query = ("SELECT data FROM {} WHERE DATE(expiredAt) =  CURDATE() AND status = 'True'".format(table_name))
        cursor.execute(query)
        messgaes = cursor.fetchall()
        if cursor.rowcount > 0:
            for msg in messgaes:
                data = msg
                query = "INSERT INTO deadlatterqueue (message) VALUES (%s)"
                cursor.execute(query, data)
                update_query = "UPDATE messages SET status = 'False' WHERE data = '{}'".format(msg[0])
                cursor.execute(update_query)
                db_conn.commit()
            print("Scheduler Job Done\n")
        db_conn.close()
